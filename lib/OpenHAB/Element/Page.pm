package OpenHAB::Element::Page;

use Moose;

extends 'OpenHAB::Element';

with 'OpenHAB::Role::HasWidgets';

use Types::OpenHAB qw( Boolean );
use Types::Standard qw( Str );

has '+id' => (
  init_arg => 'id',
);

has title => (
  is => 'rw',
  isa => Str,
);

has leaf => (
  is => 'rw',
  isa => Boolean,
  coerce => 1,
);

no Moose;

1;
