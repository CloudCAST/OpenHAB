package OpenHAB::Role::MakesObjects;

use Moose::Role;

with 'OpenHAB::Role::Base';

require OpenHAB::Item;
require OpenHAB::Element;
require OpenHAB::Sitemap;

require OpenHAB::Element::Page;
require OpenHAB::Element::Frame;
require OpenHAB::Element::Group;
require OpenHAB::Element::Text;

require OpenHAB::Item::Group;
require OpenHAB::Item::Color;
require OpenHAB::Item::Contact;
require OpenHAB::Item::Dimmer;
require OpenHAB::Item::Number;
require OpenHAB::Item::Player;
require OpenHAB::Item::Rollershutter;
require OpenHAB::Item::Switch;
require OpenHAB::Item::DateTime;
require OpenHAB::Item::String;
require OpenHAB::Item::Location;

requires 'create';

1;
