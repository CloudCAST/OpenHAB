package OpenHAB::Role::Accepts::OnOff;

require Carp;
use Moose::Role;

requires 'send';

sub turn_on  { shift->send('ON')  }
sub turn_off { shift->send('OFF') }

1;
