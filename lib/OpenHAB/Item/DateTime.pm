package OpenHAB::Item::DateTime;

use v5.10;
use Moose;
use Types::OpenHAB qw( DateTime );
use Types::Standard qw( Maybe );

extends 'OpenHAB::Item';

has '+state' => (
  isa => Maybe[DateTime],
  coerce => 1,
);

sub send {}

1;
