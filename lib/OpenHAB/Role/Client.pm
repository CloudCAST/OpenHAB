package OpenHAB::Role::Client;

use Moose::Role;
use JSON::MaybeXS;

require Carp;

with 'OpenHAB::Role::Base';

has _url => (
  is => 'ro',
  init_arg => 'url',
);

has _ua => (
  is => 'ro',
  init_arg => undef,
  lazy => 1,
  default => sub {
    require LWP::UserAgent;
    my $ua = LWP::UserAgent->new;
    $ua->default_header(
      'Accept' => 'application/json',
    );
    return $ua;
  },
);

sub get {
  my ($self, $url) = @_;

  my $response = $self->_ua->get($url);
  if (!$response->is_success) {
    Carp::croak $response->message;
  }

  return decode_json($response->decoded_content);
}

sub post {
  my ($self, $url, $data) = @_;

  my $response = $self->_ua->post(
    $url,
    'Content-Type' => 'text/plain',
    'Content' => $data,
  );
  if (!$response->is_success) {
    Carp::confess $response->message;
  }
}

1;
