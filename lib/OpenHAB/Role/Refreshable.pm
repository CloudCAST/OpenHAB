package OpenHAB::Role::Refreshable;

use Moose::Role;

with 'OpenHAB::Role::Base';

has link => ( is => 'rw' );

has is_fresh => (
  is => 'rw',
  isa => 'Bool',
  traits => ['Bool'],
  default => 0,
);

sub refresh {
  my ($self, $new) = @_;

  $new //= $self->root->client->get($self->link);

  foreach my $key (keys %{$new}) {
    my $accessor;
    foreach my $attribute (values %{$self->meta->{attributes}}) {
      $accessor = $attribute->accessor and last
        if $key eq $attribute->{init_arg};
    }

    next unless defined $accessor;

    use Ref::Util qw( is_plain_arrayref is_blessed_hashref );
    if (is_blessed_hashref $self->$accessor) {
      $self->$accessor->refresh($new->{$key});
    }
    elsif (is_plain_arrayref $new->{$key}) {
      $self->$accessor([
        map { $self->root->create_object( $_->{type}, $_ ) } @{$new->{$key}}
      ])
    }
    else {
      $self->$accessor($new->{$key});
    }
  }

  $self->is_fresh(1);
  return $self;
}

sub send {
  my ($self, $cmd) = @_;
  $self->is_fresh(0);
  $self->root->client->post( $self->link, $cmd );
  return $self->refresh;
}

1;
