package OpenHAB::Role::Accepts::NextPrevious;

use Moose::Role;

requires 'send';

sub next     { $_[0]->send('NEXT')     }
sub previous { $_[0]->send('PREVIOUS') }

1;
