package OpenHAB::Item::Group;

use Moose;

extends 'OpenHAB::Item';

with 'OpenHAB::Role::HasMembers';

around members => sub {
  my $orig = shift;
  my $self = shift;

  if (@_) {
    return $self->$orig( $self->root->factory->mass_produce( @_ ) );
  }
  else {
    return $self->$orig;
  }
};

1;
