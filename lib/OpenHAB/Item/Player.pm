package OpenHAB::Item::Player;

use v5.10;
use Moose;
use Types::OpenHAB qw( PlayPause RewindFastforward NextPrevious );
use Types::Standard qw( Maybe );

extends 'OpenHAB::Item';
with 'OpenHAB::Role::Accepts::PlayPause';
with 'OpenHAB::Role::Accepts::RewindFastforward';
with 'OpenHAB::Role::Accepts::NextPrevious';

has '+state' => (
  isa => Maybe[PlayPause|RewindFastforward|NextPrevious],
  coerce => 1,
);

around send => sub {
  my $orig = shift;
  my $self = shift;

  state $check = compile(PlayPause|RewindFastforward|NextPrevious);
  my ($hsb) = $check->(@_);
};

1;
