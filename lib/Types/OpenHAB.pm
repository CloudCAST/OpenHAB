package Types::OpenHAB;

use strict;
use warnings;

# VERSION

use DateTime;
use DateTime::Format::Strptime;

use Type::Library 0.008 -base, -declare => qw(
  Element Page Item Sitemap
  HSB DateTime String Boolean Decimal Percent Point IncreaseDecrease
  NextPrevious OnOff OpenClosed PlayPause RewindFastforward StopMove UpDown
);

use Type::Utils -all;
use Types::Standard qw( Str Num ArrayRef InstanceOf Bool );

declare Sitemap, as InstanceOf['OpenHAB::Sitemap'];
declare Item,    as InstanceOf['OpenHAB::Item'];
declare Element, as InstanceOf['OpenHAB::Element'];
declare Page,    as InstanceOf['OpenHAB::Element::Page'];

class_type DateTime, { class => "DateTime" };

declare OnOff,
  as Str, where { /^(on|off)$/i },
  message {
    "'$_' is neither ON nor OFF";
  };

declare NextPrevious,
  as Str, where { /^(next|previous)$/i },
  message {
    "'$_' is neither PLAY nor PAUSE";
  };

declare PlayPause,
  as Str, where { /^(play|pause)$/i },
  message {
    "'$_' is neither PLAY nor PAUSE";
  };

declare RewindFastforward,
  as Str, where { /^(rewind|fastforward)$/i },
  message {
    "'$_' is neither REWIND nor FASTFORWARD";
  };

declare UpDown,
  as Str, where { /^(up|down)$/i },
  message {
    "'$_' is neither UP nor DOWN";
  };

declare StopMove,
  as Str, where { /^(stop|move)$/i },
  message {
    "'$_' is neither STOP nor MOVE";
  };

declare OpenClosed,
  as Str, where { /^(open|closed)$/i },
  message {
    "'$_' is neither OPEN nor CLOSED";
  };

declare IncreaseDecrease,
  as Str, where { /^(in|de)crease$/i },
  message {
    "'$_' is neither INCREASE nor DECREASE";
  };

declare Decimal, as Num;
declare String, as Str;
declare Boolean, as Bool;

declare Percent,
  as Num, where { $_ >= 0 and $_ <= 100 },
  message {
    "'$_' is not a valid percentage";
  };

declare HSB,
  as ArrayRef[Num], where {
    return unless scalar @{$_} == 3;
    return if $_->[0] < 0 or $_->[1] < 0 or $_->[2] < 0;
    return if $_->[1] > 100 or $_->[2] > 100;
    return if $_->[0] > 360;
    return 1;
  },
  message {
    "'" . ((ref $_ eq 'ARRAY') ? join ',', @{$_} : $_) . "'" .
     " is not a valid HSB colour";
  };

declare Point,
  as ArrayRef[Num], where {
    return unless scalar @{$_} >= 2 and scalar @{$_} < 4;
    return if $_->[0] <  -90 or $_->[0] >  90;
    return if $_->[1] < -180 or $_->[1] > 180;
    return 1;
  },
  message {
    "'" . ((ref $_ eq 'ARRAY') ? join ',', @{$_} : $_) . "'" .
     " is not a valid Point";
  };

coerce(
  Point,
    from Str, via {
      [ map { 0+$_ } split(/,/, $_) ];
    },
);

coerce(
  HSB,
    from Str, via {
      [ map { 0+$_ } split(/,/, $_) ];
    },
);

coerce(
  Percent,
    from Str, via {
      return 100 if /^on$/i;
      return   0 if /^off$/i;
    },
);

coerce(
  DateTime,
    from Str, via {
      my $fmt = DateTime::Format::Strptime->new(
        pattern => '%FT%T',
        time_zone => 'GMT',
      );
      return $fmt->parse_datetime($_);
    },
);

coerce(
  Boolean,
    from Str, via {
      return 1 if /true/i;
      return 0 if /false/i;
    },
);

1;
