package OpenHAB::Role::HasItem;

use Moose::Role;
use Types::OpenHAB qw( Item );

has item => (
  is => 'rw',
  isa => Item,
);

around BUILDARGS => sub {
  my $orig = shift;
  my $self = shift;

  my $args = $self->$orig(@_);

  use Ref::Util qw( is_plain_hashref );
  if (defined $args->{item} and is_plain_hashref $args->{item}) {
    $args->{item} = $args->{root}->get_item($args->{item}{name});
  }

  return $args;
};

no Moose::Role;

1;
