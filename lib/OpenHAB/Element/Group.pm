package OpenHAB::Element::Group;

use Moose;

extends 'OpenHAB::Element';

with 'OpenHAB::Role::HasLabel';
with 'OpenHAB::Role::HasPage';
with 'OpenHAB::Role::HasItem';

no Moose;

1;
