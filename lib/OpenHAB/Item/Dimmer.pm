package OpenHAB::Item::Dimmer;

use v5.10;
use Moose;
use Types::OpenHAB qw( Percent );
use Types::Standard qw( Maybe );

extends 'OpenHAB::Item';
with 'OpenHAB::Role::Accepts::Percent';

has '+state' => (
  isa => Maybe[Percent],
  coerce => 1,
);

around send => sub {
  my $orig = shift;
  my $self = shift;

  state $check = compile(Percent);
  my ($hsb) = $check->(@_);
};

1;
