package OpenHAB::Item::String;

use v5.10;
use Moose;
use Types::OpenHAB qw( String );
use Types::Standard qw( Maybe );

extends 'OpenHAB::Item';

has '+state' => (
  isa => Maybe[String],
);

1;
