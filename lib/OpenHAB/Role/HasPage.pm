package OpenHAB::Role::HasPage;

use Moose::Role;
use Types::OpenHAB qw( Page );

has page => (
  is => 'rw',
  init_arg => 'linkedPage',
  isa => Page,
  trigger => sub {
    my ($self) = shift;
    $self->page->parent($self);
  },
);

around BUILDARGS => sub {
  my $orig = shift;
  my $self = shift;

  my $args = $self->$orig(@_);

  use Ref::Util qw( is_plain_hashref );
  if (defined $args->{linkedPage} and is_plain_hashref $args->{linkedPage} ) {

    my $type = $args->{linkedPage}->{type} // 'Page';
    $args->{linkedPage} =
      $args->{root}->create_object( $type => $args->{linkedPage} );
  }

  return $args;
};

no Moose::Role;

1;
