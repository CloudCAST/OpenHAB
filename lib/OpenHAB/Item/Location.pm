package OpenHAB::Item::Location;

use v5.10;
use Moose;
use Types::OpenHAB qw( Point );
use Types::Standard qw( Maybe );

extends 'OpenHAB::Item';

has '+state' => (
  isa => Maybe[Point],
  coerce => 1,
);

1;
