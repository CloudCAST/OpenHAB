package TestsFor::OpenHAB::Item;

use Test::Most;
use Test::Moose;
use Try::Tiny;
use base 'TestsFor::Base';

sub class_name { 'OpenHAB::Item' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  use_ok $test->class_name;
}

sub basic : Tests(no_plan) {
  my $test = shift;
  my $class = $test->class_name;

  can_ok $class, $_ foreach qw( new state );

  does_ok $class, 'OpenHAB::Role::Base';
  does_ok $class, 'OpenHAB::Role::Refreshable';
  does_ok $class, 'OpenHAB::Role::Serialisable';

  ok my $self = $class->new, 'constructor succeeds';
  isa_ok $self, $class;

  is $class->new({ state => 10 })->state, 10, 'constructor takes hashref';
  is $class->new(  state => 10  )->state, 10, 'constructor takes hash';
}

1;
