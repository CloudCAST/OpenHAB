package OpenHAB::Item;

use Moose;

with 'OpenHAB::Role::Serialisable';
with 'OpenHAB::Role::Refreshable';

has state => ( is => 'rw' );

sub BUILDARGS {
  shift;
  return (@_) ? (@_ > 1) ? { @_ } : shift : {} ;
}

1;
