package OpenHAB::Role::Element;

use Moose::Role;
use Types::Standard qw( Str );
use Types::OpenHAB qw( Sitemap Element );

has id => (
  is => 'rw',
  isa => Str,
  init_arg => 'widgetId',
);

has icon => (
  is => 'rw',
  isa => Str,
);

has parent => (
  is => 'rw',
  isa => Sitemap|Element,
  weak_ref => 1,
);

no Moose::Role;

1;
