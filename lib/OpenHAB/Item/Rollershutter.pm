package OpenHAB::Item::Rollershutter;

use v5.10;
use Moose;
use Types::OpenHAB qw( UpDown StopMove Percent );
use Types::Standard qw( Maybe );

extends 'OpenHAB::Item';
with 'OpenHAB::Role::Accepts::UpDown';
with 'OpenHAB::Role::Accepts::StopMove';
with 'OpenHAB::Role::Accepts::Percent';

has '+state' => (
  isa => Maybe[UpDown|StopMove|Percent],
  coerce => 1,
);

around send => sub {
  my $orig = shift;
  my $self = shift;

  state $check = compile(UpDown|StopMove|Percent);
  my ($hsb) = $check->(@_);
};

1;
