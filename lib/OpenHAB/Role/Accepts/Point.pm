package OpenHAB::Role::Accepts::Point;

require Carp;
use Moose::Role;

requires 'send';

before send => sub {
  my $self = shift;

  if (@_) {
    my ($lat, $lon, $alt) = split /,/, $_[0];

    Carp::cluck($_[0], " is not a valid point value")
      unless (defined $lat and defined $lon and defined $alt);
  }
};

1;
