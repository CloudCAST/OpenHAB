package OpenHAB::Role::Accepts::UpDown;

use Moose::Role;

requires 'send';

sub up   { $_[0]->send('UP') }
sub down { $_[0]->send('DOWN') }

1;
