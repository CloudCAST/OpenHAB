package OpenHAB::Element;

use Moose;
use Types::OpenHAB qw( Element );
use Types::Standard qw( ArrayRef );

with 'OpenHAB::Role::Element';
with 'OpenHAB::Role::Serialisable';
with 'OpenHAB::Role::Refreshable';

has [qw( label url )] => (
  is => 'rw',
);

sub BUILDARGS {
  shift;
  return (@_) ? (@_ > 1) ? { @_ } : shift : {} ;
}

1;
