package OpenHAB::Role::Accepts::RewindFastforward;

use Moose::Role;

requires 'send';

sub rewind      { $_[0]->send('REWIND')      }
sub fastforward { $_[0]->send('FASTFORWARD') }

1;
