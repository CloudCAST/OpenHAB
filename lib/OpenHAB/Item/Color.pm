package OpenHAB::Item::Color;

use v5.10;
use Moose;
use Types::OpenHAB qw( HSB );
use Types::Standard qw( Maybe );

extends 'OpenHAB::Item';
with 'OpenHAB::Role::Accepts::HSB';

has '+state' => (
  isa => Maybe[HSB],
  coerce => 1,
);

around send => sub {
  my $orig = shift;
  my $self = shift;

  state $check = compile(HSB);
  my ($hsb) = $check->(@_);
};

1;
