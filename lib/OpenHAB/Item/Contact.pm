package OpenHAB::Item::Contact;

use v5.10;
use Moose;
use Types::OpenHAB qw( OpenClosed );
use Types::Standard qw( Maybe );

extends 'OpenHAB::Item';
with 'OpenHAB::Role::Accepts::OpenClosed';

has '+state' => (
  isa => Maybe[OpenClosed],
  coerce => 1,
);

around send => sub {
  my $orig = shift;
  my $self = shift;

  state $check = compile(OpenClosed);
  my ($hsb) = $check->(@_);
};

1;
