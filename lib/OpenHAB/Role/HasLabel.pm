package OpenHAB::Role::HasLabel;

use Moose::Role;
use Types::Standard qw( Str );

has label => (
  is => 'rw',
  isa => Str,
);

has type  => (
  is => 'rw',
  isa => Str,
);

no Moose::Role;

1;
