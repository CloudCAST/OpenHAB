package OpenHAB::Role::Serialisable;

use Moose::Role;
use Types::Standard qw( Str );

with 'OpenHAB::Role::Base';

has [qw( name type )] => (
  is => 'ro',
  isa => Str,
);

1;
