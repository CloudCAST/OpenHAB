package OpenHAB::Role::Accepts::Decimal;

require Scalar::Util;
require Carp;
use Moose::Role;

requires 'send';

before send => sub {
  my $self = shift;
  if (@_) {
    Carp::croak($_[0], " is not a number")
      unless Scalar::Util::looks_like_number($_[0]);
  }
};

1;
