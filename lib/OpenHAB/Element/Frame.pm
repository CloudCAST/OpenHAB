package OpenHAB::Element::Frame;

use Moose;

extends 'OpenHAB::Element';

with 'OpenHAB::Role::HasWidgets';
with 'OpenHAB::Role::HasLabel';

1;
