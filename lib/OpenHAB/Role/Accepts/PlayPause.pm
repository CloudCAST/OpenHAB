package OpenHAB::Role::Accepts::PlayPause;

use Moose::Role;

requires 'send';

sub play  { $_[0]->send('PLAY')   }
sub pause { $_[0]->send('PAUSE') }

1;
