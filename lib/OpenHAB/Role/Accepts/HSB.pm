package OpenHAB::Role::Accepts::HSB;

require Carp;
use Moose::Role;
use Types::OpenHAB qw( HSB );
use Type::Params;

requires 'send';

with 'OpenHAB::Role::Accepts::Percent';

sub hsb {
  shift->send(@_);
}

1;
