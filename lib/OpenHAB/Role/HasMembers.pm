package OpenHAB::Role::HasMembers;

use Moose::Role;

has [qw( members )] => (
  is => 'rw',
  isa => 'HashRef',
);

1;
