package OpenHAB::Client;

use Moose;
use URI;
use JSON::MaybeXS qw( decode_json );
use OpenHAB::Factory;

with 'OpenHAB::Role::Client';

sub get_items {
  my $self = shift;
  my $url = URI->new_abs('rest/items', $self->_url);

  my $response = $self->_ua->get($url);
  if (!$response->is_success) {
    die $response->message;
  }

  return {
    map {
      my $o = $self->root->create_object( $_->{type}, $_ );
      $o->name => $o
    } @{decode_json($response->decoded_content)->{item}}
  };
}

sub get_sitemaps {
  my $self = shift;
  my $url = URI->new_abs('rest/sitemaps', $self->_url);

  my $response = $self->_ua->get($url);
  if (!$response->is_success) {
    die $response->message;
  }

  my $data = decode_json($response->decoded_content)->{sitemap};
  $data = [ $data ] unless ref $data eq 'ARRAY';

  return {
    map {
      my $sm = $self->root->create_object( Sitemap => $_ );
      $sm->name => $sm;
    } @{$data}
  };
}

1;
