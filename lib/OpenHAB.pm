package OpenHAB;

use Moose;
use Class::Load;
use Class::Load qw( try_load_class );

our $VERSION = '0.01';

has _url => (
  is => 'ro',
  lazy => 1,
  init_arg => 'url',
  default => 'http://localhost:8080',
);

foreach (qw( Item Sitemap ) ) {
  my $thing = lc($_);
  my $things = $thing . 's';

  has $things => (
    is => 'ro',
    isa => "HashRef[OpenHAB::$_]",
    traits => ['Hash'],
    lazy => 1,
    default => sub { {} },
    handles => {
      "get_$thing" => 'get',
      "add_$thing" => 'set',
    },
  );

  around $things => sub {
    my $orig = shift;
    my $self = shift;

    if (@_) {
      my ($id, $qr) = @_;
      my $h = $self->$orig;

      my @k =
        map { $_->name }
        grep {
          if (ref $qr eq 'Regexp') { $_->$id =~ $qr }
          else                     { $_->$id eq $qr }
        }
        values %{$h};

      my @v = @{$h}{@k};
      return { map { $k[$_] => $v[$_] } 0..$#k };
    }
    else {
      return $self->$orig;
    };
  };

  after "add_$thing" => sub {
    my ($self, $name, $elem) = @_;
    $elem->root($self)
      if $elem and $elem->root ne $self;
  };
}

foreach my $thing (qw( client factory )) {
  my $class = 'OpenHAB::' . ucfirst($thing);
  has $thing => (
    is => 'ro',
    lazy => 1,
    isa => $class,
    default => sub {
      try_load_class $class;
      $class->new(
        url  => $_[0]->_url,
        root => $_[0],
      );
    },
    trigger => sub {
      $_[0]->$thing->root($_[0]);
    },
  );
}

has '+factory' => (
  handles => {
    create_object => 'create',
  },
);

sub BUILD {
  my ($self, $args) = @_;

  foreach my $type (qw( item sitemap )) {
    my $setter = "add_$type";
    my $getter = "get_${type}s";

    foreach (values %{$self->client->$getter}) {
      $self->$setter($_->name, $_);
    }
  }
}

1;
