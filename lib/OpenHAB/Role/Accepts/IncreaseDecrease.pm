package OpenHAB::Role::Accepts::IncreaseDecrease;

use Moose::Role;

requires 'send';

sub increase { $_[0]->send('INCREASE') }
sub decrease { $_[0]->send('DECREASE') }

1;
