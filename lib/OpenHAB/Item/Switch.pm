package OpenHAB::Item::Switch;

use v5.10;
use Moose;
use Types::OpenHAB qw( OnOff );
use Types::Standard qw( Maybe );

extends 'OpenHAB::Item';
with 'OpenHAB::Role::Accepts::OnOff';

has '+state' => (
  isa => Maybe[OnOff],
  coerce => 1,
);

around send => sub {
  my $orig = shift;
  my $self = shift;

  state $check = compile(OnOff);
  my ($hsb) = $check->(@_);
};

1;
