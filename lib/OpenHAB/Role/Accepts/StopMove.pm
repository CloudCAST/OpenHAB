package OpenHAB::Role::Accepts::StopMove;

use Moose::Role;

requires 'send';

sub stop { $_[0]->send('STOP') }
sub move { $_[0]->send('MOVE') }

1;
