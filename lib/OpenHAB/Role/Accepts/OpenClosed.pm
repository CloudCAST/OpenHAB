package OpenHAB::Role::Accepts::OpenClosed;

use Moose::Role;

requires 'send';

sub open  { $_[0]->send('OPEN')   }
sub close { $_[0]->send('CLOSED') }

1;
