package OpenHAB::Factory;

use Moose;

with 'OpenHAB::Role::MakesObjects';

my @ELEMENTS = qw(
  Element Page Frame Text Group Switch Selection Setpoint
  Slider Colorpicker Chart Mapview Webview Image Video
);

sub create {
  my $self = shift;
  my $type = shift;
  my %params = (@_) ? (@_ == 1) ? %{$_[0]} : @_ : ();

  $params{root} = $self->root if defined $self->root;
  $params{state} = undef
    if defined $params{state} and $params{state} eq 'Uninitialized';

  my $class = 'OpenHAB::';
  if ($type =~ /Item$/) {
    $type =~ s/Item$//;
    $class .= 'Item::' . $type;
  }
  elsif (grep { /$type/ } @ELEMENTS) {
    $class .= 'Element::' . $type;
  }
  else {
    $class .= $type;
  }

  Carp::confess "No class!" if $class =~ /::$/;

  my $child = $class->new(%params);

  if ($child->isa('OpenHAB::Item')) {
    if ($self->root->get_item($child->name)) {
      return $self->root->get_item($child->name)->refresh(\%params);
    }
    else {
      $self->root->add_item($child->name, $child);
    }
  }
  return $child;
}

1;
