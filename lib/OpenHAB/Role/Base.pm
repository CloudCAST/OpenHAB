package OpenHAB::Role::Base;

use Moose::Role;

has [qw( root )] => (
  is => 'rw',
  weak_ref => 1,
  isa => 'OpenHAB',
);

1;
