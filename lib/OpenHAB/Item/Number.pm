package OpenHAB::Item::Number;

use v5.10;
use Moose;
use Types::OpenHAB qw( Decimal );
use Types::Standard qw( Maybe );

extends 'OpenHAB::Item';

has '+state' => (
  isa => Maybe[Decimal],
  coerce => 1,
);

1;
