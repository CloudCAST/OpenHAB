package OpenHAB::Role::HasWidgets;

use Moose::Role;
use Types::Standard qw( ArrayRef );
use Types::OpenHAB qw( Element );

has widget => (
  is => 'rw',
  isa => ArrayRef[Element],
  lazy => 1,
  traits  => ['Array'],
  default => sub { [] },
  handles => {
    add_widget => 'push',
  },
  trigger => sub {
    my ($self) = shift;
    $_->parent($self) foreach @{$self->widget};
  },
);

after add_widget => sub {
  my ($self) = shift;
  $self->widgets->[-1]->parent($self);
};

around BUILDARGS => sub {
  my $orig = shift;
  my $self = shift;

  my $args = $self->$orig(@_);

  use Ref::Util qw( is_plain_arrayref is_blessed_hashref );
  if (defined $args->{widget}) {
    $args->{widget} = [ $args->{widget} ]
      unless is_plain_arrayref $args->{widget};

    $args->{widget} = [
      map {
        (is_blessed_hashref $_)
          ? $_
          : $args->{root}->create_object( $_->{type} => $_ );
      } @{$args->{widget}}
    ];
  }

  return $args;
};

no Moose::Role;

1;
