package OpenHAB::Role::Accepts::Percent;

require Carp;
use Moose::Role;

requires 'send';

with 'OpenHAB::Role::Accepts::OnOff';
with 'OpenHAB::Role::Accepts::IncreaseDecrease';

1;
