package OpenHAB::Sitemap;

use Moose;

with 'OpenHAB::Role::Serialisable';
with 'OpenHAB::Role::Refreshable';
with 'OpenHAB::Role::HasPage';
with 'OpenHAB::Role::HasLabel';

sub BUILDARGS {
  shift;
  my $args =  (@_) ? (@_ > 1) ? { @_ } : shift : {} ;

  if (defined $args->{homepage} and !defined $args->{linkedPage}) {
    $args->{linkedPage} = delete $args->{homepage};
  }

  return $args;
}

no Moose;

1;
